/*
 * Copyright (c) 2019, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ble.h"
#include <random>
#include <cstring>
#include <iostream>
#include "esp_bt_main.h"

#define adv_config_flag      (1 << 0)
#define scan_rsp_config_flag (1 << 1)

esp_bt_controller_config_t bluedroid::ble::initializer::bt_cfg{
    ESP_TASK_BT_CONTROLLER_STACK,                   //controller_task_stack_size
    ESP_TASK_BT_CONTROLLER_PRIO,                    //controller_task_prio
    BT_HCI_UART_NO_DEFAULT,                         //hci_uart_no
    BT_HCI_UART_BAUDRATE_DEFAULT,                   //hci_uart_baudrate
    SCAN_DUPLICATE_MODE,                            //scan_duplicate_mode
    SCAN_DUPLICATE_TYPE_VALUE,                      //scan_duplicate_type
    NORMAL_SCAN_DUPLICATE_CACHE_SIZE,               //normal_adv_size
    MESH_DUPLICATE_SCAN_CACHE_SIZE,                 //mesh_adv_size
    SCAN_SEND_ADV_RESERVED_SIZE,                    //send_adv_reserved_size
    CONTROLLER_ADV_LOST_DEBUG_BIT,                  //controller_debug_flag
    BTDM_CONTROLLER_MODE_EFF,                       //mode
    CONFIG_BTDM_CTRL_BLE_MAX_CONN_EFF,              //ble_max_conn
    CONFIG_BTDM_CTRL_BR_EDR_MAX_ACL_CONN_EFF,       //bt_max_acl_conn
    CONFIG_BTDM_CTRL_BR_EDR_SCO_DATA_PATH_EFF,      //bt_sco_datapath
#ifdef BTDM_CTRL_AUTO_LATENCY_EFF
    BTDM_CTRL_AUTO_LATENCY_EFF,                     // auto_latency
#endif
    CONFIG_BTDM_CTRL_BR_EDR_MAX_SYNC_CONN_EFF,      //bt_max_sync_conn
    CONFIG_BTDM_BLE_SLEEP_CLOCK_ACCURACY_INDEX_EFF, //ble_sca
    ESP_BT_CONTROLLER_CONFIG_MAGIC_VAL              //magic
};
uint8_t bluedroid::ble::gap::initiator::adv_config_done = 0;
esp_ble_adv_params_t bluedroid::ble::gap::initiator::adv_params{
    0x20,                                           //adv_int_min        
    0x40,                                           //adv_int_max        
    ADV_TYPE_IND,                                   //adv_type           
    BLE_ADDR_TYPE_PUBLIC,                           //own_addr_type 
    {0},                                            // peer_addr
    BLE_ADDR_TYPE_PUBLIC,                           //peer_addr_type,
    ADV_CHNL_ALL,                                   //channel_map        
    ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY               //adv_filter_policy  
};
esp_ble_adv_data_t bluedroid::ble::gap::initiator::adv_data{
    false,                                          //set_scan_rsp 
    true,                                           //include_name 
    false,                                          //include_txpower 
    0x0006,                                         //min_interval 
    0x0010,                                         //max_interval  
    0x00,                                           //appearance 
    0,                                              //manufacturer_len 
    NULL,                                           //p_manufacturer_data 
    0,                                              //service_data_len 
    NULL,                                           //p_service_data 
    sizeof(adv_service_uuid128),                    //service_uuid_len 
    (uint8_t*)adv_service_uuid128,                  //p_service_uuid 
    (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT), //flag 
};
esp_ble_adv_data_t bluedroid::ble::gap::initiator::scan_rsp_data{
    true,                                           //set_scan_rsp 
    true,                                           //include_name 
    true,                                           //include_txpower 
    0,                                              //min_interval 
    0,                                              //max_interval  
    0x00,                                           //appearance 
    0,                                              //manufacturer_len 
    NULL,                                           //p_manufacturer_data 
    0,                                              //service_data_len 
    NULL,                                           //p_service_data 
    sizeof(adv_service_uuid128),                    //service_uuid_len 
    (uint8_t*)adv_service_uuid128,                  //p_service_uuid 
    (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT), //flag 
};

bool bluedroid::util::is_uuid_same(const esp_bt_uuid_t& left, const esp_bt_uuid_t& right){
    if(left.len != right.len){
        return false;
    }
    if(left.len == ESP_UUID_LEN_16){
        return left.uuid.uuid16 == right.uuid.uuid16;
    }
    if(left.len == ESP_UUID_LEN_32){
        return left.uuid.uuid32 == right.uuid.uuid32;
    }
    if(left.len == ESP_UUID_LEN_128){
        return std::equal(left.uuid.uuid128, left.uuid.uuid128+ESP_UUID_LEN_128, right.uuid.uuid128);
    }
    return false;
}


esp_err_t bluedroid::initializer::init(){
    esp_err_t ret = esp_bluedroid_init();
    if(!ret){
        ret = esp_bluedroid_enable();
    }
    return ret;
}
esp_err_t bluedroid::ble::initializer::init(){    
    esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT);
    esp_err_t ret = esp_bt_controller_init(&bt_cfg);
    if(!ret){
        ret = esp_bt_controller_enable(ESP_BT_MODE_BLE);
        if(!ret){
            bluedroid::initializer::init();
        }
    }
    return ret;
}

esp_err_t bluedroid::ble::init(){
    return initializer::init();
}

esp_err_t bluedroid::ble::gap::initiator::init(){
    esp_err_t err;
    err = esp_ble_gap_register_callback(handler);
    return err;
}
void bluedroid::ble::gap::initiator::handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t* param){
    static const char* tag = "bluedroid::ble::gap::initiator::handler";
    switch (event) {
        case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
            adv_config_done &= (~adv_config_flag);
            if (adv_config_done == 0){
                esp_ble_gap_start_advertising(&bluedroid::ble::gap::initiator::adv_params);
            }
            break;
        case ESP_GAP_BLE_SCAN_RSP_DATA_SET_COMPLETE_EVT:
            adv_config_done &= (~scan_rsp_config_flag);
            if (adv_config_done == 0){
                esp_ble_gap_start_advertising(&bluedroid::ble::gap::initiator::adv_params);
            }
            break;
        case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
            //advertising start complete event to indicate advertising start successfully or failed
            if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
                ESP_LOGE(tag, "Advertising start failed\n");
            }
            break;
        case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
            if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
                ESP_LOGE(tag, "Advertising stop failed\n");
            } else {
                ESP_LOGI(tag, "Stop adv successfully\n");
            }
            break;
        case ESP_GAP_BLE_UPDATE_CONN_PARAMS_EVT:
            ESP_LOGI(tag, "update connection params status = %d, min_int = %d, max_int = %d,conn_int = %d,latency = %d, timeout = %d",
                    param->update_conn_params.status,
                    param->update_conn_params.min_int,
                    param->update_conn_params.max_int,
                    param->update_conn_params.conn_int,
                    param->update_conn_params.latency,
                    param->update_conn_params.timeout);
            break;
        default:
            break;
    }
}
void bluedroid::ble::gap::initiator::respond(){
    static const char* tag = "bluedroid::ble::gap::initiator::respond";
    esp_err_t ret = esp_ble_gap_config_adv_data(&adv_data);
    if (ret){
        ESP_LOGE(tag, "config adv data failed, error code = %x", ret);
    }
    adv_config_done |= adv_config_flag;
    //config scan response data
    ret = esp_ble_gap_config_adv_data(&scan_rsp_data);
    if (ret){
        ESP_LOGE(tag, "config scan response data failed, error code = %x", ret);
    }
    adv_config_done |= scan_rsp_config_flag;
}

void bluedroid::ble::definitions::descriptor::create(){
    static const char* tag = "bluedroid::ble::definitions::descriptor::create";
    esp_err_t add_descr_ret = esp_ble_gatts_add_char_descr(_characteristics->_service->_handle, &_uuid, _permissions, NULL, NULL);
    if (add_descr_ret){
        ESP_LOGE(tag, "add char descr failed, error code =%x", add_descr_ret);
    }
}
std::uint16_t bluedroid::ble::definitions::descriptor::nhandles() const{
    return 1;
}

void bluedroid::ble::definitions::descriptor::process(esp_gatts_cb_event_t event, esp_ble_gatts_cb_param_t* param){
    static const char* tag = "bluedroid::ble::definitions::descriptor::process";
    switch(event){
        case ESP_GATTS_ADD_CHAR_EVT:{
            ESP_LOGI(tag, "ESP_GATTS_ADD_CHAR_EVT creating descriptor");
            create();
        }
        break;
        case ESP_GATTS_ADD_CHAR_DESCR_EVT:
            ESP_LOGI(tag, "ESP_GATTS_ADD_CHAR_DESCR_EVT setting descriptor %p handle to %d", this, param->add_char_descr.attr_handle);
            _handle = param->add_char_descr.attr_handle;
        break;
        case ESP_GATTS_READ_EVT:
            ESP_LOGI(tag, "ESP_GATTS_READ_EVT %p", this);
            esp_gatt_rsp_t rsp;
            memset(&rsp, 0, sizeof(esp_gatt_rsp_t));
            rsp.attr_value.handle = param->read.handle;
            rsp.attr_value.len = read(rsp.attr_value.value, false);
            esp_ble_gatts_send_response(_characteristics->_service->_application->_iface, param->read.conn_id, param->read.trans_id, ESP_GATT_OK, &rsp);
        break;
        case ESP_GATTS_WRITE_EVT:
            ESP_LOGI(tag, "ESP_GATTS_WRITE_EVT %p", this);
            write(param);
        break;
        default:
            break;
    }
}
std::uint16_t bluedroid::ble::definitions::descriptor::read(std::uint8_t* buff, bool streaming){
    static const char* name = "DESCRIPTOR NAME";
    static const unsigned int length = std::strlen(name);
    std::copy(name, name+length, buff);
    return length;
}
void bluedroid::ble::definitions::descriptor::write(esp_ble_gatts_cb_param_t* param){
    if(!param->write.is_prep){
        write(param->write.value, param->write.len);
    }
    esp_ble_gatts_send_response(_characteristics->_service->_application->_iface, param->write.conn_id, param->write.trans_id, ESP_GATT_OK, NULL);
}

void bluedroid::ble::definitions::descriptor::write(std::uint8_t* buff, std::uint16_t length){
    
}

bluedroid::ble::definitions::descriptors::traits::notification::notification(bluedroid::ble::definitions::descriptor& des): _descriptor(des), _state(0){}
std::uint16_t bluedroid::ble::definitions::descriptors::traits::notification::read(std::uint8_t* buff){
    std::memcpy(buff, reinterpret_cast<const char*>(&_state), sizeof(std::uint16_t));
    return sizeof(std::uint16_t);
}

void bluedroid::ble::definitions::descriptors::traits::notification::write(std::uint8_t* buff, std::uint16_t length){
    static const char* tag = "bluedroid::ble::definitions::descriptors::traits::notification::write";

    ESP_LOGI(tag, "GATT_WRITE_EVT, value len %d, value :", length);
    esp_log_buffer_hex(tag, buff, length);
    
    if(length == 2){
        uint16_t descr_value = buff[1]<<8 | buff[0];
        if (descr_value == 0x0001){
            if (_descriptor._characteristics->_property & ESP_GATT_CHAR_PROP_BIT_NOTIFY){
                _state = 0x0001;
                ESP_LOGI(tag, "notify enable");
                _descriptor._characteristics->push();
                _descriptor._notifying = true;
                _descriptor._indicating = false;
            }
        }else if (descr_value == 0x0002){
            if (_descriptor._characteristics->_property & ESP_GATT_CHAR_PROP_BIT_INDICATE){
                _state = 0x0002;
                ESP_LOGI(tag, "indicate enable NOOP");
                _descriptor._indicating = true;
                _descriptor._notifying = false;
            }
        }else if (descr_value == 0x0000){
            _state = 0x0000;
            ESP_LOGI(tag, "notify/indicate disable");
            _descriptor._notifying = false;
            _descriptor._indicating = false;
        }else{
            _state = 0x0000;
            ESP_LOGE(tag, "unknown descr value");
            esp_log_buffer_hex(tag, buff, length);
        }
    }
    if(_descriptor._notifying){
        _descriptor._characteristics->start();
    }else{
        _descriptor._characteristics->stop();
    }
}



void bluedroid::ble::definitions::characteristics::create(){
    esp_ble_gatts_add_char(_service->_handle, &_uuid, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY, &_value, NULL);
    // esp_ble_gatts_add_char(_service->_handle, &_uuid, _permissions, _property, &_value, NULL);
}
std::uint16_t bluedroid::ble::definitions::characteristics::nhandles() const{
    std::uint16_t sum =   1 /* characteristics declaration*/ 
                        + 1 /* characteristics value */; 
    for(bluedroid::ble::definitions::descriptor* descriptor: _descriptors){
        sum += descriptor->nhandles();
    }
    return sum;
}
void bluedroid::ble::definitions::characteristics::add(bluedroid::ble::definitions::descriptor* descriptor){
    descriptor->_characteristics = this;
    _descriptors.push_back(descriptor);
}
bluedroid::ble::definitions::descriptor* bluedroid::ble::definitions::characteristics::by_uuid(esp_bt_uuid_t uuid) const{
    for(bluedroid::ble::definitions::descriptor* descriptor: _descriptors){
        if(util::is_uuid_same(descriptor->_uuid, uuid)){
            return descriptor;
        }
    }
    return 0x0;
}
bool bluedroid::ble::definitions::characteristics::has_descriptor(esp_bt_uuid_t uuid) const{
    return by_uuid(uuid) != 0x0;
}
bluedroid::ble::definitions::descriptor* bluedroid::ble::definitions::characteristics::by_handle(std::uint16_t h) const{
    for(bluedroid::ble::definitions::descriptor* descriptor: _descriptors){
        if(descriptor->_handle == h){
            return descriptor;
        }
    }
    return 0x0;
}
bool bluedroid::ble::definitions::characteristics::has_handle(std::uint16_t h) const{
    return by_handle(h) != 0x0;
}
void bluedroid::ble::definitions::characteristics::start(){
    _notify = true;
}
void bluedroid::ble::definitions::characteristics::stop(){
    _notify = false;
}
void bluedroid::ble::definitions::characteristics::push(){    
    if(_notify){
        uint8_t notify_data[ESP_GATT_MAX_ATTR_LEN];
        std::uint16_t length = read(notify_data, true);
        ESP_LOGI("bluedroid::ble::definitions::characteristics::push", "pushing data to the central device");
        esp_ble_gatts_send_indicate(_service->_application->_iface, _service->_application->_connection, _handle, length, notify_data, false);
    }
}

std::uint16_t bluedroid::ble::definitions::characteristics::read(std::uint8_t* buff, bool streaming){
    buff[0] = (rand() % (127 - 0 + 1)) + 0;
    if(!streaming){
        buff[1] = (rand() % (127 - 0 + 1)) + 0;
        return 2;
    }
    return 1;
}
void bluedroid::ble::definitions::characteristics::write(std::uint8_t* buff, std::uint16_t length){
    
}



#define PREPARE_BUF_MAX_SIZE 1024
void bluedroid::ble::definitions::characteristics::write_pending(prepare_type_env_t* prepare_write_env, esp_ble_gatts_cb_param_t* param){
    static const char* tag = "bluedroid::ble::definitions::characteristics::write";
    esp_gatt_status_t status = ESP_GATT_OK;
    if (param->write.need_rsp){
        if (param->write.is_prep){
            ESP_LOGI(tag, "param->write.is_prep");
            if (prepare_write_env->prepare_buf == NULL) {
                prepare_write_env->prepare_buf = (uint8_t *)malloc(PREPARE_BUF_MAX_SIZE*sizeof(uint8_t));
                prepare_write_env->prepare_len = 0;
                if (prepare_write_env->prepare_buf == NULL) {
                    ESP_LOGE(tag, "Gatt_server prep no mem\n");
                    status = ESP_GATT_NO_RESOURCES;
                }
            } else {
                if(param->write.offset > PREPARE_BUF_MAX_SIZE) {
                    status = ESP_GATT_INVALID_OFFSET;
                } else if ((param->write.offset + param->write.len) > PREPARE_BUF_MAX_SIZE) {
                    status = ESP_GATT_INVALID_ATTR_LEN;
                }
            }

            esp_gatt_rsp_t *gatt_rsp = (esp_gatt_rsp_t *)malloc(sizeof(esp_gatt_rsp_t));
            gatt_rsp->attr_value.len = param->write.len;
            gatt_rsp->attr_value.handle = param->write.handle;
            gatt_rsp->attr_value.offset = param->write.offset;
            gatt_rsp->attr_value.auth_req = ESP_GATT_AUTH_REQ_NONE;
            memcpy(gatt_rsp->attr_value.value, param->write.value, param->write.len);
            esp_err_t response_err = esp_ble_gatts_send_response(_service->_application->_iface, param->write.conn_id, param->write.trans_id, status, gatt_rsp);
            if (response_err != ESP_OK){
               ESP_LOGE(tag, "Send response error\n");
            }
            free(gatt_rsp);
            if (status != ESP_GATT_OK){
                return;
            }
            memcpy(prepare_write_env->prepare_buf + param->write.offset, param->write.value, param->write.len);
            prepare_write_env->prepare_len += param->write.len;
        }else{
            ESP_LOGI(tag, "NOT param->write.is_prep");
            esp_ble_gatts_send_response(_service->_application->_iface, param->write.conn_id, param->write.trans_id, status, NULL);
        }
    }else{
        ESP_LOGI(tag, "NOT param->write.need_rsp");
    }
}

void bluedroid::ble::definitions::characteristics::process(esp_gatts_cb_event_t event, esp_ble_gatts_cb_param_t* param){
    static const char* tag = "bluedroid::ble::definitions::characteristics::process";
    switch(event){
        case ESP_GATTS_CREATE_EVT:
            ESP_LOGI(tag, "ESP_GATTS_REG_EVT creating characteristics");
            create();
        break;
        case ESP_GATTS_ADD_CHAR_EVT:{
            _handle = param->add_char.attr_handle;
            ESP_LOGI(tag, "ESP_GATTS_ADD_CHAR_EVT setting characteristic %p with handle %d", this, _handle);
            ESP_LOGI(tag, "ESP_GATTS_ADD_CHAR_EVT adding descriptor");
            uint16_t length = 0;
            const uint8_t *prf_char;
            esp_err_t get_attr_ret = esp_ble_gatts_get_attr_value(param->add_char.attr_handle,  &length, &prf_char);
            if (get_attr_ret == ESP_FAIL){
                ESP_LOGE(tag, "ILLEGAL HANDLE");
            }
            ESP_LOGI(tag, "the gatts demo char length = %x\n", length);
            for(int i = 0; i < length; i++){
                ESP_LOGI(tag, "prf_char[%x] =%x",i,prf_char[i]);
            }
            for(bluedroid::ble::definitions::descriptor* descriptor: _descriptors){
                descriptor->process(event, param);
            }
        }
        break;
        case ESP_GATTS_ADD_CHAR_DESCR_EVT:
            ESP_LOGI(tag, "ESP_GATTS_ADD_CHAR_DESCR_EVT setting descriptor handle to %d", param->add_char_descr.attr_handle);
            for(bluedroid::ble::definitions::descriptor* descriptor: _descriptors){
                if(util::is_uuid_same(descriptor->_uuid, param->add_char_descr.descr_uuid)){
                    descriptor->process(event, param);
                }
            }
        break;
        case ESP_GATTS_READ_EVT: {
            if(param->read.handle == _handle){
                ESP_LOGI(tag, "GATT_READ_EVT responding");
                esp_gatt_rsp_t rsp;
                memset(&rsp, 0, sizeof(esp_gatt_rsp_t));
                rsp.attr_value.handle = param->read.handle;
                rsp.attr_value.len = read(rsp.attr_value.value, false);
                esp_ble_gatts_send_response(_service->_application->_iface, param->read.conn_id, param->read.trans_id, ESP_GATT_OK, &rsp);
            }else{
                for(bluedroid::ble::definitions::descriptor* descriptor: _descriptors){
                    if(descriptor->_handle == param->read.handle){
                        ESP_LOGI(tag, "GATT_READ_EVT redirecting to descriptor %p", descriptor);
                        descriptor->process(event, param);
                    }
                }
            }
        }
        break;
        case ESP_GATTS_WRITE_EVT: {
            if(param->write.handle == _handle){
                bool notify = false;
                if(!param->write.is_prep){
                    ESP_LOGI(tag, "GATT_WRITE_EVT, value len %d, value :", param->write.len);
                    esp_log_buffer_hex(tag, param->write.value, param->write.len);
                    
                    if(param->write.len == 2){
                        uint16_t descr_value = param->write.value[1]<<8 | param->write.value[0];
                        if (descr_value == 0x0001){
                            if (_property & ESP_GATT_CHAR_PROP_BIT_NOTIFY){
                                ESP_LOGI(tag, "notify enable");
                                push();
                                notify = true;
                            }
                        }else if (descr_value == 0x0002){
                            if (_property & ESP_GATT_CHAR_PROP_BIT_INDICATE){
                                ESP_LOGI(tag, "indicate enable");
                                uint8_t indicate_data[15];
                                for (int i = 0; i < sizeof(indicate_data); ++i){
                                    indicate_data[i] = i%0xff;
                                }
                                //the size of indicate_data[] need less than MTU size
                                esp_ble_gatts_send_indicate(_service->_application->_iface, param->write.conn_id, _handle, sizeof(indicate_data), indicate_data, true);
                            }
                        }else if (descr_value == 0x0000){
                            ESP_LOGI(tag, "notify/indicate disable");
                            notify = false;
                        }else{
                            ESP_LOGE(tag, "unknown descr value");
                            esp_log_buffer_hex(tag, param->write.value, param->write.len);
                        }
                    }
                }
                write_pending(&_prepared, param);
                if(notify){
                    start();
                }else{
                    stop();
                }
                write(param->write.value, param->write.len);
            }else{
                for(bluedroid::ble::definitions::descriptor* descriptor: _descriptors){
                    if(descriptor->_handle == param->read.handle){
                        ESP_LOGI(tag, "GATT_WRITE_EVT redirecting to descriptor %p", descriptor);
                        descriptor->process(event, param);
                    }
                }
            }
        }
        break;
        case ESP_GATTS_EXEC_WRITE_EVT:
            if(param->write.handle == _handle){
                ESP_LOGI(tag,"ESP_GATTS_EXEC_WRITE_EVT");
                esp_ble_gatts_send_response(_service->_application->_iface, param->write.conn_id, param->write.trans_id, ESP_GATT_OK, NULL);
                if (param->exec_write.exec_write_flag == ESP_GATT_PREP_WRITE_EXEC){
                    esp_log_buffer_hex(tag, _prepared.prepare_buf, _prepared.prepare_len);
                }else{
                    ESP_LOGI(tag,"ESP_GATT_PREP_WRITE_CANCEL");
                }
                if (_prepared.prepare_buf) {
                    free(_prepared.prepare_buf);
                    _prepared.prepare_buf = NULL;
                }
                _prepared.prepare_len = 0;
            }else{
                for(bluedroid::ble::definitions::descriptor* descriptor: _descriptors){
                    if(descriptor->_handle == param->read.handle){
                        ESP_LOGI(tag, "ESP_GATTS_EXEC_WRITE_EVT redirecting to descriptor %p", descriptor);
                        descriptor->process(event, param);
                    }
                }
            }
        break;
        default:
            break;
    }
}

std::uint16_t bluedroid::ble::definitions::service::nhandles() const{
    std::uint16_t sum = 1;
    for(bluedroid::ble::definitions::characteristics* characteristics: _characteristics){
        sum += characteristics->nhandles();
    }
    return sum;
}

void bluedroid::ble::definitions::service::add(bluedroid::ble::definitions::characteristics* characteristics){
    characteristics->_service = this;
    _characteristics.push_back(characteristics);
}
bluedroid::ble::definitions::characteristics* bluedroid::ble::definitions::service::by_handle(std::uint16_t h) const{
    static const char* tag = "bluedroid::ble::definitions::service::by_handle";
    for(bluedroid::ble::definitions::characteristics* characteristics: _characteristics){
        if(characteristics->_handle == h || characteristics->has_handle(h)){
            return characteristics;
        }
    }
    ESP_LOGE(tag, "no characteristics found for handle %d", h);
    for(bluedroid::ble::definitions::characteristics* characteristics: _characteristics){
        ESP_LOGE(tag, "characteristics %p handle %d", characteristics, characteristics->_handle);
    }
    return 0x0;
}
bluedroid::ble::definitions::characteristics* bluedroid::ble::definitions::service::by_uuid(esp_bt_uuid_t uuid) const{
    static const char* tag = "bluedroid::ble::definitions::service::by_uuid";
    for(bluedroid::ble::definitions::characteristics* characteristics: _characteristics){
        if(util::is_uuid_same(characteristics->_uuid, uuid)){
            return characteristics;
        }
    }
    ESP_LOGE(tag, "no characteristics found for uuid %d", uuid.uuid.uuid16);
    return 0x0;
}
bool bluedroid::ble::definitions::service::has_characteristics(std::uint16_t h) const{
    return by_handle(h) != 0x0;
}
bool bluedroid::ble::definitions::service::has_characteristics(esp_bt_uuid_t uuid) const{
    return by_uuid(uuid) != 0x0;
}

void bluedroid::ble::definitions::service::create(){
    esp_ble_gatts_create_service(_application->_iface, &_id, nhandles());
}
void bluedroid::ble::definitions::service::start(){
    esp_ble_gatts_start_service(_handle);
}
void bluedroid::ble::definitions::service::process(esp_gatts_cb_event_t event, esp_ble_gatts_cb_param_t* param){
    static const char* tag = "bluedroid::ble::definitions::service::process";
    switch(event){
        case ESP_GATTS_REG_EVT:
            ESP_LOGI(tag, "ESP_GATTS_REG_EVT creating service");
            create();
        break;
        case ESP_GATTS_CREATE_EVT:
            _handle = param->create.service_handle;
            start();
            ESP_LOGI(tag, "ESP_GATTS_CREATE_EVT starting service %p with handle %d", this, _handle);
            for(bluedroid::ble::definitions::characteristics* characteristics: _characteristics){
                ESP_LOGI(tag, "ESP_GATTS_CREATE_EVT redirecting to characteristics %p", characteristics);
                characteristics->process(event, param);
            }
        break;
        case ESP_GATTS_ADD_CHAR_EVT:{
            auto characteristics = by_uuid(param->add_char.char_uuid);
            ESP_LOGI(tag, "ESP_GATTS_ADD_CHAR_EVT redirecting to characteristics %p", characteristics);
            characteristics->process(event, param);
        }
        break;
        case ESP_GATTS_ADD_CHAR_DESCR_EVT:{
            for(bluedroid::ble::definitions::characteristics* characteristics: _characteristics){
                if(characteristics->has_descriptor(param->add_char_descr.descr_uuid)){
                    ESP_LOGI(tag, "ESP_GATTS_ADD_CHAR_DESCR_EVT redirecting to characteristics %p", characteristics);
                    characteristics->process(event, param);
                }
            }
        }
        break;
        case ESP_GATTS_READ_EVT: {
            bluedroid::ble::definitions::characteristics* characteristics = by_handle(param->read.handle);
            ESP_LOGI(tag, "GATT_READ_EVT redirecting to characteristics %p", characteristics);
            characteristics->process(event, param);
        }
        break;
        case ESP_GATTS_WRITE_EVT: {
            bluedroid::ble::definitions::characteristics* characteristics = by_handle(param->write.handle);
            ESP_LOGI(tag, "ESP_GATTS_WRITE_EVT redirecting to characteristics %p", characteristics);
            characteristics->process(event, param);
        }
        break;
        case ESP_GATTS_EXEC_WRITE_EVT: {
            bluedroid::ble::definitions::characteristics* characteristics = by_handle(param->write.handle);
            ESP_LOGI(tag, "ESP_GATTS_EXEC_WRITE_EVT redirecting to characteristics %p", characteristics);
            characteristics->process(event, param);
        }
        break;
        default:
            break;
    }
}

bluedroid::ble::definitions::application::application(std::uint32_t stack_size){
    xTaskCreate(bluedroid::ble::definitions::application::produce, "produce", stack_size, this, 5, NULL);
}
void bluedroid::ble::definitions::application::add(bluedroid::ble::definitions::service* service){
    service->_application = this;
    _services.push_back(service);
}
bluedroid::ble::definitions::service* bluedroid::ble::definitions::application::by_handle(std::uint16_t h) const{
    for(bluedroid::ble::definitions::service* service: _services){
        if(service->_handle == h){
            return service;
        }
    }
    return 0x0;
}
bluedroid::ble::definitions::service* bluedroid::ble::definitions::application::by_uuid(esp_bt_uuid_t uuid) const{
    for(bluedroid::ble::definitions::service* service: _services){
        if(util::is_uuid_same(service->_id.id.uuid, uuid)){
            return service;
        }
    }
    return 0x0;
}
bool bluedroid::ble::definitions::application::has_service(std::uint16_t h) const{
    return by_handle(h);
}
bool bluedroid::ble::definitions::application::has_service(esp_bt_uuid_t uuid) const{
    return by_uuid(uuid) != 0x0;
}
void bluedroid::ble::definitions::application::produce(void* data){
    bluedroid::ble::definitions::application* app = reinterpret_cast<bluedroid::ble::definitions::application*>(data);
    app->produce();
}
void bluedroid::ble::definitions::application::produce(){
    while(1){
        for(bluedroid::ble::definitions::service* service: _services){
            for(bluedroid::ble::definitions::characteristics* characteristics: service->_characteristics){
                characteristics->push();
            }
        }
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
void bluedroid::ble::definitions::application::process(esp_gatts_cb_event_t event, esp_ble_gatts_cb_param_t* param){
    static const char* tag = "bluedroid::ble::definitions::application::process";
    switch (event) {
        case ESP_GATTS_REG_EVT:{            
            for(bluedroid::ble::definitions::service* service: _services){
                ESP_LOGI(tag, "ESP_GATTS_REG_EVT redirecting to service %p", service);
                service->process(event, param);
            }
        }
        break;
        case ESP_GATTS_CREATE_EVT:{
            auto service = by_uuid(param->create.service_id.id.uuid);
            ESP_LOGI(tag, "ESP_GATTS_CREATE_EVT redirecting to service %p", service);
            service->process(event, param);
        }
        break;
        case ESP_GATTS_ADD_CHAR_EVT:{
            auto service = by_handle(param->add_char.service_handle);
            ESP_LOGI(tag, "ESP_GATTS_ADD_CHAR_EVT redirecting to service %p", service);
            service->process(event, param);
        }
        break;
        case ESP_GATTS_ADD_CHAR_DESCR_EVT:{
            auto service = by_handle(param->add_char_descr.service_handle);
            ESP_LOGI(tag, "ESP_GATTS_ADD_CHAR_DESCR_EVT redirecting to service %p", service);
            service->process(event, param);
        }
        break;
        case ESP_GATTS_CONNECT_EVT:{
            esp_ble_conn_update_params_t conn_params{{0}, 0, 0, 0, 0};
            std::memset(&conn_params, 0, sizeof(esp_ble_conn_update_params_t));
            std::memcpy(conn_params.bda, param->connect.remote_bda, sizeof(esp_bd_addr_t));
            conn_params.latency = 0;
            conn_params.max_int = 0x20;    // max_int = 0x20*1.25ms = 40ms
            conn_params.min_int = 0x10;    // min_int = 0x10*1.25ms = 20ms
            conn_params.timeout = 400;    // timeout = 400*10ms = 4000ms
            ESP_LOGI(tag, "ESP_GATTS_CONNECT_EVT, conn_id %d, remote %02x:%02x:%02x:%02x:%02x:%02x:", param->connect.conn_id, param->connect.remote_bda[0], param->connect.remote_bda[1], param->connect.remote_bda[2], param->connect.remote_bda[3], param->connect.remote_bda[4], param->connect.remote_bda[5]);
            _connection = param->connect.conn_id;
            ESP_LOGI(tag, "ESP_GATTS_CONNECT_EVT setting connection id to %d", _connection);
            esp_ble_gap_update_conn_params(&conn_params);
        }
        break;
        case ESP_GATTS_READ_EVT: {
            ESP_LOGI(tag, "GATT_READ_EVT, conn_id %d, trans_id %d, handle %d", param->read.conn_id, param->read.trans_id, param->read.handle);
            for(bluedroid::ble::definitions::service* service: _services){
                if(service->has_characteristics(param->read.handle)){
                    ESP_LOGI(tag, "GATT_READ_EVT redirecting to service %p", service);
                    service->process(event, param);
                }
            }
            break;
        }
        case ESP_GATTS_WRITE_EVT: {
            ESP_LOGI(tag, "GATT_WRITE_EVT, conn_id %d, trans_id %d, handle %d", param->write.conn_id, param->write.trans_id, param->write.handle);
            for(bluedroid::ble::definitions::service* service: _services){
                if(service->has_characteristics(param->write.handle)){
                    ESP_LOGI(tag, "GATT_WRITE_EVT redirecting to service %p", service);
                    service->process(event, param);
                }
            }
        }
        break;
        case ESP_GATTS_EXEC_WRITE_EVT: {
            ESP_LOGI(tag, "ESP_GATTS_EXEC_WRITE_EVT, conn_id %d, trans_id %d, handle %d", param->write.conn_id, param->write.trans_id, param->write.handle);
            for(bluedroid::ble::definitions::service* service: _services){
                if(service->has_characteristics(param->write.handle)){
                    ESP_LOGI(tag, "ESP_GATTS_EXEC_WRITE_EVT redirecting to service %p", service);
                    service->process(event, param);
                }
            }
        }
        break;
        case ESP_GATTS_DISCONNECT_EVT:
            ESP_LOGI(tag, "ESP_GATTS_DISCONNECT_EVT, disconnect reason 0x%x", param->disconnect.reason);
            esp_ble_gap_start_advertising(&bluedroid::ble::gap::initiator::adv_params);
            break;
        case ESP_GATTS_CONF_EVT:
            ESP_LOGI(tag, "ESP_GATTS_CONF_EVT, status %d attr_handle %d", param->conf.status, param->conf.handle);
            if (param->conf.status != ESP_GATT_OK){
                esp_log_buffer_hex(tag, param->conf.value, param->conf.len);
            }
            break;
        default:
            break;
    }
}

bluedroid::ble::gatt::initiator::applications_container_type bluedroid::ble::gatt::initiator::_applications;
esp_err_t bluedroid::ble::gatt::initiator::init(){
    esp_err_t err;
    ESP_LOGI(tag, "registering gatt callback");
    err = esp_ble_gatts_register_callback(handler);
    if(!err){
        for(int i = 0; i < bluedroid::ble::gatt::initiator::_applications.size(); ++i){
            err = esp_ble_gatts_app_register(i);
            ESP_LOGI(tag, "registering app with id %d", i);
        }
    }
    return err;
}
bluedroid::ble::gatt::initiator::size_type bluedroid::ble::gatt::initiator::add(bluedroid::ble::definitions::application* app){
    _applications.push_back(app);
    return _applications.size();
}

void bluedroid::ble::gatt::initiator::handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t* param){
    static const char* tag = "bluedroid::ble::gatt::initiator::handler";
    if (event == ESP_GATTS_REG_EVT) {
        if (param->reg.status == ESP_GATT_OK) {
            ESP_LOGI(tag, "REGISTER_APP_EVT, status %d, app_id %d", param->reg.status, param->reg.app_id);
            bluedroid::ble::gatt::initiator::_applications[param->reg.app_id]->_iface = gatts_if;
            esp_err_t set_dev_name_ret = esp_ble_gap_set_device_name(TEST_DEVICE_NAME);
            if (set_dev_name_ret){
                ESP_LOGE(tag, "set device name failed, error code = %x", set_dev_name_ret);
            }
            gap::initiator::respond();
        } else {
            ESP_LOGI(tag, "Reg app failed, app_id %04x, status %d", param->reg.app_id, param->reg.status);
            return;
        }
    }
    for (bluedroid::ble::definitions::application* app: bluedroid::ble::gatt::initiator::_applications) {
        if (gatts_if == ESP_GATT_IF_NONE || gatts_if == app->_iface) {
            // pass message to appropriate application
            app->process(event, param);
        }
    }
}


