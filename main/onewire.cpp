/*
 * Copyright (c) 2019, <copyright holder> <email>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <copyright holder> <email> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> <email> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "onewire.h"

void water::ds18b20::init(OneWireBus* bus){
    _bus = bus;
}
void water::ds18b20::setup(OneWireBus_ROMCode rom_code, bool solo){
    DS18B20_Info* ds18b20_info = ds18b20_malloc();  // heap allocation
    if(solo){
        ds18b20_init_solo(ds18b20_info, _bus); // associate with bus and device
    }else{
        ds18b20_init(ds18b20_info, _bus, rom_code); // associate with bus and device
    }
    ds18b20_use_crc(ds18b20_info, true);           // enable CRC check for temperature readings
    ds18b20_set_resolution(ds18b20_info, DS18B20_RESOLUTION_12_BIT);
    char rom_code_s[17];
    owb_string_from_rom_code(rom_code, rom_code_s, sizeof(rom_code_s));
    _devices.insert(std::make_pair(std::string(rom_code_s), ds18b20_info));
}
water::ds18b20::~ds18b20(){
    std::cout << "dtor" << std::endl;
    for(auto dev: _devices){
        ds18b20_free(&dev.second);
    }
    owb_uninitialize(_bus);
}
water::ds18b20::data_type water::ds18b20::read(const std::string& code){
    vTaskDelay(pdMS_TO_TICKS(1000));
    ds18b20_convert_all(_bus);
    ds18b20_wait_for_conversion(_devices.begin()->second);
    data_type reading;
    DS18B20_ERROR error = ds18b20_read_temp(_devices[code], &reading);
    if(error != DS18B20_OK){
        std::cout << "ERROR: " << error << std::endl;
    }
    std::cout << code << ": " << reading << std::endl;
    return reading;
}
void water::ds18b20::read_all(){
    for(auto dev: _devices){
        data_type value = read(dev.first);
        _data[dev.first] = value;
    }
}

water::ds18b20::data_type water::ds18b20::at(const std::string signature) const{
    return _data.at(signature);
}

water::ds18b20::data_type water::ds18b20::at(std::size_t index) const{
    auto it = _data.cbegin();
    std::advance(it, index);
    return it->second;
}
