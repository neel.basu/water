/*
 * Copyright (c) 2019, <copyright holder> <email>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <copyright holder> <email> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> <email> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WATER_NIMBLE_H
#define WATER_NIMBLE_H

#include <iostream>
#include <cstdint>
#include <cstring>
#include <tuple>
#include <array>
#include "nimble/ble.h"
#include "modlog/modlog.h"
#include "host/ble_hs.h"
#include "host/ble_uuid.h"
#include "esp_nimble_hci.h"
#include "nimble/nimble_port.h"
#include "services/gap/ble_svc_gap.h"
#include "services/gatt/ble_svc_gatt.h"
#include "nimble/nimble_port_freertos.h"

// void print_addr(const void* addr){
//     const uint8_t *u8p;
// 
//     u8p = static_cast<const uint8_t *>(addr);
//     
// }

namespace nimble{
namespace options{
    struct services{
        struct types{
            static constexpr uint8_t primary = BLE_GATT_SVC_TYPE_PRIMARY;
            static constexpr uint8_t secondary = BLE_GATT_SVC_TYPE_SECONDARY;
            static constexpr uint8_t end = BLE_GATT_SVC_TYPE_END;
        };
    };
    struct characteristics{
        struct flags{
            static constexpr ble_gatt_chr_flags broadcast = BLE_GATT_CHR_F_BROADCAST;
            static constexpr ble_gatt_chr_flags read = BLE_GATT_CHR_F_READ;
            static constexpr ble_gatt_chr_flags write_no_rsp = BLE_GATT_CHR_F_WRITE_NO_RSP;
            static constexpr ble_gatt_chr_flags write = BLE_GATT_CHR_F_WRITE;
            static constexpr ble_gatt_chr_flags notify = BLE_GATT_CHR_F_NOTIFY;
            static constexpr ble_gatt_chr_flags indicate = BLE_GATT_CHR_F_INDICATE;
            static constexpr ble_gatt_chr_flags auth_sign_write = BLE_GATT_CHR_F_AUTH_SIGN_WRITE;
            static constexpr ble_gatt_chr_flags reliable_write = BLE_GATT_CHR_F_RELIABLE_WRITE;
            static constexpr ble_gatt_chr_flags aux_write = BLE_GATT_CHR_F_AUX_WRITE;
            static constexpr ble_gatt_chr_flags read_enc = BLE_GATT_CHR_F_READ_ENC;
            static constexpr ble_gatt_chr_flags read_authen = BLE_GATT_CHR_F_READ_AUTHEN;
            static constexpr ble_gatt_chr_flags read_author = BLE_GATT_CHR_F_READ_AUTHOR;
            static constexpr ble_gatt_chr_flags write_enc = BLE_GATT_CHR_F_WRITE_ENC;
            static constexpr ble_gatt_chr_flags write_authen = BLE_GATT_CHR_F_WRITE_AUTHEN;
            static constexpr ble_gatt_chr_flags write_author = BLE_GATT_CHR_F_WRITE_AUTHOR;
        };
    };
}
    
namespace detail{
    namespace tuple{
        template <size_t> struct SizeT{};
        template <typename TupleType, typename ActionType>
        inline void for_each(TupleType& tuple, ActionType action){
            for_each(tuple, action, SizeT<std::tuple_size<TupleType>::value>());
        }
        template <typename TupleType, typename ActionType>
        inline void for_each(TupleType& tuple, ActionType action, SizeT<0>){}
        template <typename TupleType, typename ActionType, size_t N>
        inline void for_each(TupleType& tuple, ActionType action, SizeT<N>){
            for_each(tuple, action, SizeT<N-1>());
            action(std::get<N-1>(tuple));
        }
    }
}
    
namespace definitions{   
    template <typename T, std::uint16_t UUID, ble_gatt_chr_flags Flags>
    struct characteristics: T{
        typedef characteristics<T, UUID, Flags> self_type;
        
        ble_uuid16_t uuid;
        std::uint16_t handle;
        ble_gatt_chr_def basic_def;
        
        characteristics(): T(handle){
            std::cout << __FUNCTION__ << ": " << std::hex << "0x" << UUID << std::endl;
            uuid.u.type = BLE_UUID_TYPE_16;
            uuid.value = UUID;
            
            basic_def.uuid = (const ble_uuid_t *)&uuid;
            std::cout << std::hex << ble_uuid_u16(basic_def.uuid) << std::endl;
            basic_def.flags = Flags;
            basic_def.val_handle = &handle;
            basic_def.arg = this;
            basic_def.descriptors = NULL;
            basic_def.min_key_size = 0;
            basic_def.access_cb = &self_type::rw;
        }       
        static int rw(std::uint16_t conn_handle, std::uint16_t attr_handle, struct ble_gatt_access_ctxt* ctxt, void* arg){
            self_type* target = reinterpret_cast<self_type*>(arg);
            return target->access(conn_handle, attr_handle, ctxt);
        }
        ble_gatt_chr_def def() const{
            return basic_def;
        }
    };
   
    template <std::uint16_t UUID, std::uint8_t Type, typename... Characteristics>
    struct service{           
        enum{
            count = sizeof...(Characteristics)
        };
        typedef service<UUID, Type, Characteristics...> self_type;
        typedef struct ble_gatt_chr_def characteristics_definition_type;
        typedef std::tuple<Characteristics...> tuple_type;
        typedef characteristics_definition_type container_type[count+1];
        
        static constexpr ble_uuid16_t uuid{ble_uuid_t{BLE_UUID_TYPE_16}, UUID};
        
        tuple_type _characteristics;
        container_type _characteristics_c;
        ble_gatt_svc_def basic_def;
        
        service(){
            std::memset(_characteristics_c, 0, sizeof(_characteristics_c));
            int i = 0;
            detail::tuple::for_each(_characteristics, [this, &i](const auto& chr){
                ble_gatt_chr_def chr_def = chr.def();
                std::memcpy(&_characteristics_c[i++], &chr_def, sizeof (struct ble_gatt_chr_def) ); 
                std::cout << "service::service() / detail::tuple::for_each:" << std::hex << " /chr.uuid.value: 0x" << chr.uuid.value << " /ble_uuid_u16(_characteristics_c[i-1]->uuid): 0x" << ble_uuid_u16(_characteristics_c[i-1].uuid) << std::endl;
            });
            
            basic_def.type = Type;
            basic_def.uuid = reinterpret_cast<const ble_uuid_t *>(&uuid);
            basic_def.includes = NULL;
            basic_def.characteristics = _characteristics_c;
        }
        ble_gatt_svc_def def() const{
            return basic_def;
        }
    };
    template <typename... Services>
    struct peripheral{
        enum{
            count = sizeof...(Services)
        };
        
        typedef peripheral<Services...> self_type;
        typedef ble_gatt_svc_def service_definition_type;
        typedef std::tuple<Services...> tuple_type;
        typedef service_definition_type container_type[count+1];
        
        tuple_type _services;
        container_type _services_c;
        
        peripheral(){
            std::memset(_services_c, 0, sizeof(_services_c));
            int i = 0;
            detail::tuple::for_each(_services, [this, &i](const auto& svc){
                ble_gatt_svc_def svc_def = svc.def();
                std::memcpy(&_services_c[i++], &svc_def, sizeof (struct ble_gatt_svc_def) ); 
                std::cout << "peripheral::peripheral() / detail::tuple::for_each" << ": " << std::hex << "0x" << svc.uuid.value << std::endl;
            });
        }
        const ble_gatt_svc_def* def() const{
            return _services_c;
        }
    };
}

    template <typename DeviceT, typename... Services>
    struct peripheral{
        typedef peripheral<DeviceT, Services...> self_type;
        typedef definitions::peripheral<Services...> definition_type;
        
        static self_type* _self;        
        definition_type definition;
        const ble_gatt_svc_def* def;
        static uint8_t blehr_addr_type;
        static uint16_t conn_handle;
        static constexpr const char *device_name = "Water Quality Sensor";
        
        
        peripheral(){
            def = definition.def();
            self_type::init(this);
        }
        
        static void init(self_type* self){
            self_type::_self = self;
            
            nimble_port_init();
            ble_hs_cfg.sync_cb = self_type::sync;
            ble_hs_cfg.reset_cb = self_type::reset;
            
            int rc;
            rc = self->gatt_svr_init();
            assert(rc == 0);

            /* Set the default device name */
            rc = ble_svc_gap_device_name_set(device_name);
            assert(rc == 0);

            /* Start the task */
            nimble_port_freertos_init(self_type::blehr_host_task);
        }
        static void sync(void){
            int rc;
            rc = ble_hs_id_infer_auto(0, &blehr_addr_type);
            assert(rc == 0);

            uint8_t addr_val[6] = {0};
            rc = ble_hs_id_copy_addr(blehr_addr_type, addr_val, NULL);

            MODLOG_DFLT(INFO, "Device Address: ");
            MODLOG_DFLT(INFO, "%02x:%02x:%02x:%02x:%02x:%02x", addr_val[5], addr_val[4], addr_val[3], addr_val[2], addr_val[1], addr_val[0]);
            // print_addr(addr_val);
            MODLOG_DFLT(INFO, "\n");

            /* Begin advertising */
            _self->advertise();
        }
        static void reset(int reason){
            MODLOG_DFLT(ERROR, "Resetting state; reason=%d\n", reason);
        }
        static void blehr_host_task(void *param){
            ESP_LOGI("Heart Rate", "BLE Host Task Started");
            /* This function will return only when nimble_port_stop() is executed */
            nimble_port_run();

            nimble_port_freertos_deinit();
        }
        
        void advertise(){
            struct ble_gap_adv_params adv_params;
            struct ble_hs_adv_fields fields;
            int rc;

            /*
            *  Set the advertisement data included in our advertisements:
            *     o Flags (indicates advertisement type and other general info)
            *     o Advertising tx power
            *     o Device name
            */
            memset(&fields, 0, sizeof(fields));

            /*
            * Advertise two flags:
            *      o Discoverability in forthcoming advertisement (general)
            *      o BLE-only (BR/EDR unsupported)
            */
            fields.flags = BLE_HS_ADV_F_DISC_GEN | BLE_HS_ADV_F_BREDR_UNSUP;

            /*
            * Indicate that the TX power level field should be included; have the
            * stack fill this value automatically.  This is done by assigning the
            * special value BLE_HS_ADV_TX_PWR_LVL_AUTO.
            */
            fields.tx_pwr_lvl_is_present = 1;
            fields.tx_pwr_lvl = BLE_HS_ADV_TX_PWR_LVL_AUTO;

            fields.name = (uint8_t *)device_name;
            fields.name_len = strlen(device_name);
            fields.name_is_complete = 1;

            rc = ble_gap_adv_set_fields(&fields);
            if (rc != 0) {
                MODLOG_DFLT(ERROR, "error setting advertisement data; rc=%d\n", rc);
                return;
            }

            /* Begin advertising */
            memset(&adv_params, 0, sizeof(adv_params));
            adv_params.conn_mode = BLE_GAP_CONN_MODE_UND;
            adv_params.disc_mode = BLE_GAP_DISC_MODE_GEN;
            rc = ble_gap_adv_start(blehr_addr_type, NULL, BLE_HS_FOREVER, &adv_params, gap_event, this);
            if (rc != 0) {
                MODLOG_DFLT(ERROR, "error enabling advertisement; rc=%d\n", rc);
                return;
            }
        }
        static int gap_event(struct ble_gap_event* event, void* arg){
            self_type* self = static_cast<self_type*>(arg);
            return self->gap_event(event);
        }
        int gap_event(struct ble_gap_event* event){
            switch (event->type) {
                case BLE_GAP_EVENT_CONNECT:
                    /* A new connection was established or a connection attempt failed */
                    MODLOG_DFLT(INFO, "connection %s; status=%d\n",
                                event->connect.status == 0 ? "established" : "failed",
                                event->connect.status);

                    if (event->connect.status != 0) {
                        /* Connection failed; resume advertising */
                        advertise();
                    }
                    conn_handle = event->connect.conn_handle;
                    break;

                case BLE_GAP_EVENT_DISCONNECT:
                    MODLOG_DFLT(INFO, "disconnect; reason=%d\n", event->disconnect.reason);

                    /* Connection terminated; resume advertising */
                    advertise();
                    break;

                case BLE_GAP_EVENT_ADV_COMPLETE:
                    MODLOG_DFLT(INFO, "adv complete\n");
                    advertise();
                    break;

                case BLE_GAP_EVENT_SUBSCRIBE:
                    MODLOG_DFLT(INFO, "subscribe event; conn_handle=%d attr_handle=%d reason=%d prevn=%d curn=%d previ=%d curi=%d\n",
                    event->subscribe.conn_handle,
                    event->subscribe.attr_handle,
                    event->subscribe.reason,
                    event->subscribe.prev_notify,
                    event->subscribe.cur_notify,
                    event->subscribe.prev_indicate,
                    event->subscribe.cur_indicate);
    //                 MODLOG_DFLT(INFO, "subscribe event; cur_notify=%d\n value handle; val_handle=%d\n", event->subscribe.cur_notify, hrs_hrm_handle);
    //                 if (event->subscribe.attr_handle == hrs_hrm_handle){
    //                     notify_state = event->subscribe.cur_notify;
    //                     blehr_tx_hrate_reset();
    //                 } else if (event->subscribe.attr_handle != hrs_hrm_handle){
    //                     notify_state = event->subscribe.cur_notify;
    //                     blehr_tx_hrate_stop();
    //                 }
                    ESP_LOGI("BLE_GAP_SUBSCRIBE_EVENT", "conn_handle from subscribe=%d", conn_handle);
                    break;

                case BLE_GAP_EVENT_MTU:
                    MODLOG_DFLT(INFO, "mtu update event; conn_handle=%d mtu=%d\n", event->mtu.conn_handle, event->mtu.value);
                    break;

            }

            return 0;
        }
        int gatt_svr_init(void){
            int rc;

            ble_svc_gap_init();
            ble_svc_gatt_init();
            
            std::cout << "-------------------------------------------------------------" << std::endl;
            
            std::cout << "CHECK " << std::hex << ble_uuid_u16(def->characteristics->uuid) << std::endl;
            std::cout << "CHECK A " << std::hex << def->characteristics->arg << std::endl;
            std::cout << "CHECK D " << std::hex << def->characteristics->descriptors << std::endl;
            
            std::cout << "1CHECK " << std::hex << ble_uuid_u16((def->characteristics+1)->uuid) << std::endl;
            std::cout << "1CHECK A " << std::hex << (def->characteristics+1)->arg << std::endl;
            std::cout << "1CHECK D " << std::hex << (def->characteristics+1)->descriptors << std::endl;
                      
            rc = ble_gatts_count_cfg(def);
            if (rc != 0) {
                return rc;
            }

            rc = ble_gatts_add_svcs(def);
            if (rc != 0) {
                return rc;
            }

            return 0;
        }
    };
    template <typename DeviceT, typename... Services>
    uint8_t peripheral<DeviceT, Services...>::blehr_addr_type = 0;
    template <typename DeviceT, typename... Services>
    uint16_t peripheral<DeviceT, Services...>::conn_handle = 0;
    template <typename DeviceT, typename... Services>
    peripheral<DeviceT, Services...>* peripheral<DeviceT, Services...>::_self = 0x0;
}


#endif // WATER_NIMBLE_H
