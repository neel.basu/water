/*
 * Copyright (c) 2019, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WATER_BLE_H
#define WATER_BLE_H

#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_bt.h"
#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include <cstring>
#include <vector>
#include <cstdint>
#include <string>

#define TEST_DEVICE_NAME "Water Quality Node"

namespace bluedroid{
    
namespace util{
bool is_uuid_same(const esp_bt_uuid_t& left, const esp_bt_uuid_t& right);
}
    
struct initializer{
    static esp_err_t init();
};
    
namespace ble{
    
typedef struct {
    uint8_t                 *prepare_buf;
    int                     prepare_len;
} prepare_type_env_t;
    
struct initializer{
    static esp_bt_controller_config_t bt_cfg;
    static esp_err_t init();
};

esp_err_t init();
    
template <int Size>
struct uuid;

template <>
struct uuid<16>{
    enum{
        length = ESP_UUID_LEN_16
    };
    esp_bt_uuid_t _data;
    
    uuid(std::uint16_t value){
        _data.len = length;
        _data.uuid.uuid16 = value;
    }
    operator esp_bt_uuid_t() const{
        return _data;
    }
};
template <>
struct uuid<128>{
    enum{
        length = ESP_UUID_LEN_128
    };
    esp_bt_uuid_t _data;
    
    uuid(std::uint16_t value){
        _data.len = length;
        _data.uuid.uuid16 = value;
    }
    operator esp_bt_uuid_t() const{
        return _data;
    }
};

namespace definitions{
    struct service;
    struct application;
    struct characteristics;
    
    struct defs{
        static constexpr const esp_gatt_perm_t permissions = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE;
        static constexpr const esp_gatt_char_prop_t property = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY;
        static constexpr const uint8_t data[] = {0x11,0x22,0x33};
        static constexpr const esp_attr_value_t value{0x40, sizeof(data), (uint8_t*)data};
    };
    
    struct descriptor{
        esp_bt_uuid_t        _uuid;
        std::uint16_t        _handle;
        esp_gatt_perm_t      _permissions;
        esp_attr_value_t     _value;
        esp_attr_control_t   _control;
        bool                 _notifying;
        bool                 _indicating;
        
        characteristics*     _characteristics;
        
        template <int N>
        descriptor(uuid<N> id, esp_gatt_perm_t perm = defs::permissions, esp_attr_value_t value = defs::value): _notifying(false){
            _uuid = id;
            _permissions = perm;
            _value = value;
        }
        void create();
        void process(esp_gatts_cb_event_t event, esp_ble_gatts_cb_param_t* param);
        std::uint16_t nhandles() const;
        
        virtual std::uint16_t read(std::uint8_t* buff, bool streaming);
        virtual void write(esp_ble_gatts_cb_param_t* param);
        virtual void write(std::uint8_t* buff, std::uint16_t length);
    };
    
    namespace descriptors{
        namespace traits{
            struct notification{
                descriptor& _descriptor;
                std::uint16_t _state;
                
                notification(descriptor& des);
                std::uint16_t read(std::uint8_t* buff);
                void write(std::uint8_t* buff, std::uint16_t length);
            };
            template <typename ValueT>
            struct readable_property{
                descriptor& _descriptor;
                ValueT _property;
                
                readable_property(descriptor& des, const ValueT& value): _descriptor(des), _property(value){}
                std::uint16_t read(std::uint8_t* buff){
                    std::memcpy(buff, reinterpret_cast<const char*>(&_property), sizeof(ValueT));
                    return sizeof(ValueT);
                }
            };
            template <>
            struct readable_property<std::string>{
                descriptor& _descriptor;
                std::string _property;
                
                readable_property(descriptor& des, const std::string& value): _descriptor(des), _property(value){}
                std::uint16_t read(std::uint8_t* buff){
                    auto len = _property.length();
                    const char* data = _property.data();
                    std::memcpy(buff, data, len);
                    return len;
                }
            };
            template <typename ValueT>
            struct mutable_property: readable_property<ValueT>{
                mutable_property(descriptor& des, const ValueT& value): readable_property<ValueT>(des, value){}
                void write(std::uint8_t* buff, std::uint16_t length){
                    if(length <= sizeof(ValueT)){
                        const char* data = reinterpret_cast<const char*>(&readable_property<ValueT>::_property);
                        std::memcpy(data, buff, length);
                    }else{
                        ESP_LOGE("mutable_property::read", "write with size %d requested where expected size is <= %d", length, sizeof(ValueT));
                    }
                }
            };
            template <>
            struct mutable_property<std::string>: readable_property<std::string>{
                mutable_property(descriptor& des, const std::string& value): readable_property<std::string>(des, value){}
                void write(std::uint8_t* buff, std::uint16_t length){
                    readable_property<std::string>::_property.assign(reinterpret_cast<const char*>(buff), static_cast<std::string::size_type>(length));
                }
            };
            template <typename ValueT>
            struct immutable_property: readable_property<ValueT>{
                using readable_property<ValueT>::readable_property;
                
                void write(std::uint8_t* buff, std::uint16_t length){}
            };
        }
    }
    
    struct characteristics{
        typedef std::vector<bluedroid::ble::definitions::descriptor*> container_type;
        
        esp_bt_uuid_t        _uuid;
        esp_gatt_perm_t      _permissions;
        esp_attr_control_t   _control;
        esp_gatt_char_prop_t _property;
        esp_attr_value_t     _value;
        std::uint16_t        _handle;
        prepare_type_env_t   _prepared;
        container_type       _descriptors;
        bool                 _notify;
        
        service*             _service;
        
        template <int N>
        characteristics(uuid<N> id, esp_gatt_char_prop_t prop = defs::property, esp_gatt_perm_t perm = defs::permissions, esp_attr_value_t value = defs::value){
            _uuid = id;
            _permissions = perm;
            _property = prop;
            _value = value;
            
            stop();
        }
        void create();
        void process(esp_gatts_cb_event_t event, esp_ble_gatts_cb_param_t* param);
        void write_pending(prepare_type_env_t* prepare_write_env, esp_ble_gatts_cb_param_t *param);
        std::uint16_t nhandles() const;
        void add(bluedroid::ble::definitions::descriptor* descriptor);
        
        bluedroid::ble::definitions::descriptor* by_uuid(esp_bt_uuid_t uuid) const;
        bool has_descriptor(esp_bt_uuid_t uuid) const;
        bluedroid::ble::definitions::descriptor* by_handle(std::uint16_t h) const;
        bool has_handle(std::uint16_t h) const;
        
        void start();
        void stop();
        void push();
        
        virtual std::uint16_t read(std::uint8_t* buff, bool streaming);
        virtual void write(std::uint8_t* buff, std::uint16_t length);
    };
    
    struct service{
        typedef std::vector<bluedroid::ble::definitions::characteristics*> container_type;
        
        esp_gatt_srvc_id_t  _id;
        std::uint16_t       _handle;
        container_type      _characteristics;
        
        application*        _application;
        
        template <int N>
        service(uuid<N> id, bool primary = true){
            _id.is_primary = primary;
            _id.id.inst_id = 0x0;
            _id.id.uuid = id;
        }
        void create();
        void process(esp_gatts_cb_event_t event, esp_ble_gatts_cb_param_t* param);
        void start();
        std::uint16_t nhandles() const;
        void add(bluedroid::ble::definitions::characteristics* characteristics);
        
        characteristics* by_handle(std::uint16_t h) const;
        bool has_characteristics(std::uint16_t h) const;
        characteristics* by_uuid(esp_bt_uuid_t uuid) const;
        bool has_characteristics(esp_bt_uuid_t uuid) const;
    };
    
    struct application{
        typedef std::vector<bluedroid::ble::definitions::service*> container_type;
        
        esp_gatt_if_t   _iface;
        uint16_t        _connection;
        container_type  _services;
        
        application(std::uint32_t stack_size=3192);
        void add(bluedroid::ble::definitions::service* service);
        void process(esp_gatts_cb_event_t event, esp_ble_gatts_cb_param_t* param);
        
        service* by_handle(std::uint16_t h) const;
        bool has_service(std::uint16_t h) const;
        service* by_uuid(esp_bt_uuid_t uuid) const;
        bool has_service(esp_bt_uuid_t uuid) const;
        
        static void produce(void* data);
        void produce();
    };
}

struct chars_trait{
    enum class types{
        none,
        readonly,
        writable,
        notifiable
    };
};

template <std::uint16_t UUID, typename T>
struct characteristics: definitions::characteristics, T{
    typedef characteristics<UUID, T> self_type;
    
    template <typename... U>
    characteristics(U... args): definitions::characteristics(bluedroid::ble::uuid<16>(UUID)), T(args...){}
    template <typename... U>
    characteristics(U... args, esp_gatt_char_prop_t prop): definitions::characteristics(bluedroid::ble::uuid<16>(UUID), prop), T(args...){}
    template <typename... U>
    characteristics(U... args, esp_gatt_char_prop_t prop, esp_gatt_perm_t perm): definitions::characteristics(bluedroid::ble::uuid<16>(UUID), prop, perm), T(args...){}
    template <typename... U>
    characteristics(U... args, esp_gatt_char_prop_t prop, esp_gatt_perm_t perm, esp_attr_value_t value): definitions::characteristics(bluedroid::ble::uuid<16>(UUID), prop, perm, value), T(args...){}
    std::uint16_t read(std::uint8_t* buff, bool streaming){
        auto value = T::read(streaming);
        std::memcpy(buff, reinterpret_cast<const char*>(&value), sizeof(value));
        return sizeof(value);
    }
    void write(std::uint8_t* buff, std::uint16_t length){
        definitions::characteristics::write(buff, length);
        T::write(buff, length);
    }
    static self_type* create(esp_gatt_perm_t perm = definitions::defs::permissions, esp_gatt_char_prop_t prop = definitions::defs::property, esp_attr_value_t value = definitions::defs::value){
        return new self_type(perm, prop, value);
    }
};
template <std::uint16_t UUID>
struct descriptor: definitions::descriptor{
    typedef descriptor<UUID> self_type;
    
    descriptor(esp_gatt_perm_t perm = definitions::defs::permissions, esp_attr_value_t value = definitions::defs::value): definitions::descriptor(bluedroid::ble::uuid<16>(UUID), perm, value){}
    static self_type* create(esp_gatt_perm_t perm = definitions::defs::permissions, esp_attr_value_t value = definitions::defs::value){
        return new self_type(perm, value);
    }
};
template <>
struct descriptor<ESP_GATT_UUID_CHAR_CLIENT_CONFIG>: definitions::descriptor, definitions::descriptors::traits::notification{
    typedef descriptor<ESP_GATT_UUID_CHAR_CLIENT_CONFIG> self_type;
    descriptor(esp_attr_value_t value = definitions::defs::value): definitions::descriptor(bluedroid::ble::uuid<16>(ESP_GATT_UUID_CHAR_CLIENT_CONFIG), ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, value), definitions::descriptors::traits::notification(static_cast<definitions::descriptor&>(*this)){}
    static self_type* create(esp_attr_value_t value = definitions::defs::value){
        return new self_type(value);
    }
    std::uint16_t read(std::uint8_t* buff, bool streaming){
        return definitions::descriptors::traits::notification::read(buff);
    }
    void write(std::uint8_t* buff, std::uint16_t length){
        definitions::descriptors::traits::notification::write(buff, length);
    }
};

template <>
struct descriptor<ESP_GATT_UUID_CHAR_DESCRIPTION>: definitions::descriptor, definitions::descriptors::traits::immutable_property<std::string>{
    typedef descriptor<ESP_GATT_UUID_CHAR_DESCRIPTION> self_type;
    descriptor(const std::string& name, esp_attr_value_t value = definitions::defs::value): definitions::descriptor(bluedroid::ble::uuid<16>(ESP_GATT_UUID_CHAR_DESCRIPTION), ESP_GATT_PERM_READ, value), definitions::descriptors::traits::immutable_property<std::string>(static_cast<definitions::descriptor&>(*this), name){}
    static self_type* create(const std::string& name, esp_attr_value_t value = definitions::defs::value){
        return new self_type(name, value);
    }
    std::uint16_t read(std::uint8_t* buff, bool streaming){
        return definitions::descriptors::traits::immutable_property<std::string>::read(buff);
    }
    void write(std::uint8_t* buff, std::uint16_t length){
        definitions::descriptors::traits::immutable_property<std::string>::write(buff, length);
    }
};
template <std::uint16_t UUID, typename T>
struct parameter: characteristics<UUID, T>{
    bluedroid::ble::descriptor<ESP_GATT_UUID_CHAR_DESCRIPTION> descriptor_name;
    bluedroid::ble::descriptor<ESP_GATT_UUID_CHAR_CLIENT_CONFIG> descriptor_config;
    
    template <typename... U>
    parameter(const std::string& name, U... args): characteristics<UUID, T>(args...), descriptor_name(name){
        characteristics<UUID, T>::add(&descriptor_name);
        characteristics<UUID, T>::add(&descriptor_config);
    }
};
template <std::uint16_t UUID>
struct service: definitions::service{
    typedef service<UUID> self_type;
    
    service(bool primary = true): definitions::service(bluedroid::ble::uuid<16>(UUID)){}
    static self_type* create(bool primary = true){
        return new self_type(primary);
    }
};
struct application: definitions::application{
    typedef application self_type;
    
    using definitions::application::application;
    
    static self_type* create(std::uint32_t stack_size = 3192){
        return new self_type(stack_size);
    }
};

namespace io{
    template <typename T, int IDX=-1>
    struct reader{
        const T& _sensor;
        
        reader(const T& sensor): _sensor(sensor){}
        std::int32_t read(bool streaming){
            return _sensor[IDX]*100;
        }
        void write(std::uint8_t* buff, std::uint16_t length){}
    };
    template <typename T>
    struct reader<T, -1>{
        const T& _sensor;

        reader(const T& sensor): _sensor(sensor){}
        std::int32_t read(bool streaming){
            return _sensor()*100;
        }
        void write(std::uint8_t* buff, std::uint16_t length){}
    };
}
 
// ESP_GATTS_REG_EVT
//     service: create
// ESP_GATTS_CREATE_EVT:
//     service: handle
//     service: start
//     characteristics: create
// ESP_GATTS_ADD_CHAR_EVT:
//     characteristics: handle
//     descriptor: create
// ESP_GATTS_ADD_CHAR_DESCR_EVT:
//     descriptor: handle
    

 
namespace gap{   
    struct initiator{
        static constexpr uint8_t adv_service_uuid128[32] = {
            /* LSB <--------------------------------------------------------------------------------> MSB */
            //first uuid, 16bit, [12],[13] is the value
            0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xEE, 0x00, 0x00, 0x00,
            //second uuid, 32bit, [12], [13], [14], [15] is the value
            0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,
        };
        static uint8_t adv_config_done;
        static esp_ble_adv_params_t adv_params;
        static esp_ble_adv_data_t adv_data;
        static esp_ble_adv_data_t scan_rsp_data;
        
        static esp_err_t init();
        static void handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t* param);
        static void respond();
    };
}

namespace gatt{
    struct initiator{
        typedef std::vector<bluedroid::ble::definitions::application*> applications_container_type;
        typedef typename applications_container_type::size_type size_type;
        
        static constexpr const char* tag = "gatt::initiator::handler";
        static applications_container_type _applications;
        
        static esp_err_t init();
        static size_type add(bluedroid::ble::definitions::application* app);
        static void handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);
    };
}
    


}
}



#endif // WATER_BLE_H
