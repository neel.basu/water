/*
 * Copyright (c) 2019, <copyright holder> <email>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <copyright holder> <email> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> <email> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WATER_APPLICATION_H
#define WATER_APPLICATION_H

#include <iostream>
#include "analog_reader.h"
#include "onewire.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "onewire.h"
#include "ble.h"

namespace water{
namespace calibrations{
    struct pH{
        typedef double data_type;
    
        data_type operator()(std::uint32_t mV) const;
    };
}
}

using namespace water;


struct application{
    typedef readers::calibrated::buffered::adc1<calibrations::pH> ph_reader_type;
    typedef bluedroid::ble::io::reader<ph_reader_type> ph_ble_reader_type;
    typedef readers::onewire<water::ds18b20> temperature_reader_type;
    typedef bluedroid::ble::io::reader<temperature_reader_type> temperature_ble_reader_type;
    
    ph_reader_type pH;
    temperature_reader_type temperature;
       
    bluedroid::ble::parameter<0xFF01, ph_ble_reader_type> param_pH;
    bluedroid::ble::service<0x00FF> service_sensing;
    bluedroid::ble::application app_ble;
        
    application();
    
    /**
     * \brief produces sensed data
     * reads the sensor and stores it inside the circular buffer
     */
    int produce();
    /**
     * main loop
     */
    int main();
    
    static void produce(void* arg);
};

#endif // WATER_APPLICATION_H
