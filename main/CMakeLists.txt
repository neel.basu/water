set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(COMPONENT_SRCS "main.c" "ble.cpp" "mainx.cpp" "onewire.cpp" "application.cpp")
set(COMPONENT_ADD_INCLUDEDIRS ".")

register_component()
