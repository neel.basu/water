/*
 * Copyright (c) 2019, <copyright holder> <email>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <copyright holder> <email> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> <email> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WATER_ANALOG_READER_H
#define WATER_ANALOG_READER_H

#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include <cstdint>
#include <tuple>
#include <functional>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include "onewire.h"

#define DEFAULT_VREF    1100        //Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES   64          //Multisampling

namespace water{
namespace readers{

/**
 * @todo write docs
 */
template <adc_unit_t ADCUnit>
struct raw_analog_reader;

template <>
struct raw_analog_reader<ADC_UNIT_1>{
    typedef std::uint32_t data_type;
    typedef adc1_channel_t channel_type;
    typedef raw_analog_reader<ADC_UNIT_1> self_type;
  
    adc1_channel_t _channel;
    
    raw_analog_reader(adc1_channel_t channel): _channel(channel){
        init();
    }
    raw_analog_reader(const self_type& other) = delete;
    void init(){
        adc1_config_width(ADC_WIDTH_BIT_12);
        adc1_config_channel_atten(_channel, ADC_ATTEN_DB_11);
    }
    int read() const{
        return adc1_get_raw(_channel);
    }
};

template <>
struct raw_analog_reader<ADC_UNIT_2>{
    typedef std::int32_t data_type;
    typedef adc2_channel_t channel_type;
    typedef raw_analog_reader<ADC_UNIT_2> self_type;
    
    adc2_channel_t _channel;
    
    raw_analog_reader(adc2_channel_t channel): _channel(channel){
        init();
    }
    raw_analog_reader(const self_type& other) = delete;
    void init(){
        adc2_config_channel_atten(_channel, ADC_ATTEN_DB_11);
    }
    int read() const{
        int raw;
        adc2_get_raw(_channel, ADC_WIDTH_BIT_12, &raw);
        return raw;
    }
    data_type operator()(){
        return read();
    }
};

template <adc_unit_t ADCUnit>
struct voltage_reader: protected raw_analog_reader<ADCUnit>{
    typedef raw_analog_reader<ADCUnit> raw_reader;
    typedef std::uint32_t data_type;
    typedef voltage_reader<ADCUnit> self_type;
    
    esp_adc_cal_characteristics_t* _characteristics;
    
    voltage_reader(int pin): raw_reader((typename raw_reader::channel_type)pin){
        characterize();
    }
    voltage_reader(const self_type& other) = delete;
    esp_adc_cal_value_t characterize(){
        _characteristics = (esp_adc_cal_characteristics_t*)calloc(1, sizeof(esp_adc_cal_characteristics_t));
        esp_adc_cal_value_t val_type = esp_adc_cal_characterize(ADCUnit, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, DEFAULT_VREF, _characteristics);
        return val_type;
    }
    int raw() const{
        return raw_reader::read();
    }
    std::uint32_t voltage() const{
        int adc_reading = raw();
        return esp_adc_cal_raw_to_voltage(adc_reading, _characteristics);
    }
    data_type operator()() const{
        return voltage();
    }
};

template <typename CalibrationT, typename ReaderT>
struct calibrated_reader: protected ReaderT, protected CalibrationT{
    typedef ReaderT reader_type;
    typedef CalibrationT calibration_type;
    typedef typename calibration_type::data_type data_type;
    typedef calibrated_reader<calibration_type, reader_type> self_type;
    
    calibrated_reader(const self_type& other) = delete;
    
    using reader_type::reader_type;
    using reader_type::voltage;
    using reader_type::raw;
    data_type operator()() const{
        typename reader_type::data_type voltage = reader_type::voltage();
        return calibration_type::operator()(voltage);
    }
};

namespace calibrated{
    /**
     * water::readers::calibrated::adc1<calibrations::pH> pH(2);
     * pH();
     */
    template <typename CalibrationT>
    using adc1 = calibrated_reader<CalibrationT, voltage_reader<ADC_UNIT_1>>;
    /**
     * water::readers::calibrated::adc2 temperature(2);
     * temperature.voltage();
     */
    template <typename CalibrationT>
    using adc2 = calibrated_reader<CalibrationT, voltage_reader<ADC_UNIT_2>>;
}

template <typename ReaderT>
struct buffered_reader: protected ReaderT{
    typedef ReaderT reader_type;
    typedef typename reader_type::data_type data_type;
    typedef buffered_reader<reader_type> self_type;

    enum{
        max = 32
    };

    std::size_t _size;
    bool _ready;
    data_type _buff[max];

    template <typename U>
    buffered_reader(U u): reader_type(u), _size(0), _ready(false){
        std::fill(_buff, _buff+max, data_type(0));
    }
    buffered_reader(const self_type& other) = delete;
    void read_one(){
        if(_size == max){
            _ready = true;
            _size = 0;
        }
        _buff[_size++] = reader_type::operator()();
    }
    std::size_t length() const{
        return _ready ? std::size_t(max) : _size;
    }
    data_type mean() const{
        return std::accumulate(_buff, _buff+length(), data_type(0)) / length();
    }
    using reader_type::voltage;
    using reader_type::raw;
    data_type operator()() const{
        return mean();
    }
    template <typename StreamT>
    StreamT& write(StreamT& stream) const{
        stream << std::setw(12) << mean() << " mV: " << std::setw(4) << voltage() << " raw: " << std::setw(4) << raw();
        return stream;
    }
};

template <typename StreamT, typename ReaderT>
StreamT& operator<<(StreamT& stream, const buffered_reader<ReaderT>& reader){
  reader.write(stream);
  return stream;
}

template <typename DeviceT>
struct buffered_reader<onewire<DeviceT>>: protected onewire<DeviceT>{
  typedef onewire<DeviceT> reader_type;
  typedef typename onewire<DeviceT>::data_type data_type;
  
  enum{
    max = 32
  };
  
  std::size_t _size;
  bool _ready;
  data_type _buff[max];
  std::string _signature;
  
  template <typename U>
  buffered_reader(U u, const std::string& signature): reader_type(u), _size(0), _ready(false), _signature(signature){
    std::fill(_buff, _buff+max, data_type(0));
  }
  template <typename U>
  buffered_reader(U u, int index): reader_type(u), _size(0), _ready(false){
    std::fill(_buff, _buff+max, data_type(0));
    _signature = reader_type::signature(index);
  }
  void read_one(){
    if(_size == max){
      _ready = true;
      _size = 0;
    }
    reader_type::fetch();
    _buff[_size++] = reader_type::at(_signature);
  }
  std::size_t length() const{
    return _ready ? std::size_t(max) : _size;
  }
  data_type mean() const{
    return std::accumulate(_buff, _buff+length(), data_type(0)) / length();
  }
  data_type operator()() const{
    return mean();
  }
  template <typename StreamT>
  StreamT& write(StreamT& stream) const{
    stream << std::setw(12) << mean();
    return stream;
  }
};

namespace buffered{
    template <typename DeviceT>
    using onewire = buffered_reader<onewire<DeviceT>>;
}

namespace calibrated {
    namespace buffered{
	/**
	* water::readers::calibrated::adc1<calibrations::pH> pH(2);
	* pH();
	*/
	template <typename CalibrationT>
	using adc1 = buffered_reader<calibrated_reader<CalibrationT, voltage_reader<ADC_UNIT_1>>>;
	/**
	* water::readers::calibrated::adc2 temperature(2);
	* temperature.voltage();
	*/
	template <typename CalibrationT>
	using adc2 = buffered_reader<calibrated_reader<CalibrationT, voltage_reader<ADC_UNIT_2>>>;
    }  
}

}
}

#endif // WATER_ANALOG_READER_H
