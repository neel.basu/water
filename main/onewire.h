/*
 * Copyright (c) 2019, <copyright holder> <email>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <copyright holder> <email> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> <email> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WATER_ONEWIRE_H
#define WATER_ONEWIRE_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "owb.h"
#include "owb_rmt.h"
#include "ds18b20.h"
#include <cstdint>
#include <array>
#include <string>
#include <map>
#include <iostream>
#include <cstring>

#define MAX_DEVICES     8

namespace water{

struct ds18b20{
    typedef float data_type;
    
    OneWireBus* _bus;
    std::map<std::string, DS18B20_Info*> _devices;
    std::map<std::string, data_type> _data;
    
    void init(OneWireBus* bus);
    void setup(OneWireBus_ROMCode rom_code, bool solo = false);
    ~ds18b20();
    data_type read(const std::string& code);
    void read_all();
    data_type at(const std::string signature) const;
    data_type at(std::size_t index) const;
    template <typename StreamT>
    StreamT& write(StreamT& stream){
        for(auto dev: _devices){
            stream << dev.first << ": " << _data[dev.first] << std::endl;
        }
        return stream;
    }
};
    
namespace readers{
/**
 * @todo write docs
 */
template <typename DeviceT>
struct onewire: protected DeviceT{
    typedef typename DeviceT::data_type data_type;
    
    uint8_t _pin;
    owb_rmt_driver_info _rmt_driver_info;
    OneWireBus* _bus;
    std::array<OneWireBus_ROMCode, MAX_DEVICES> _rom_codes;
    int _ndevices;
    
    onewire(std::uint8_t pin): _pin(pin), _rmt_driver_info(owb_rmt_driver_info()), _bus(0x0), _ndevices(0){
        vTaskDelay(2000.0 / portTICK_PERIOD_MS);
        _bus = owb_rmt_initialize(&_rmt_driver_info, _pin, RMT_CHANNEL_1, RMT_CHANNEL_0);
        owb_use_crc(_bus, true);
        search();
        DeviceT::init(_bus);
        if(_ndevices > 0){
            init();
        }
    }
    void search(){        
        OneWireBus_SearchState search_state = {};
        std::memset(&search_state, 0, sizeof(OneWireBus_SearchState));
        bool found = false;
        owb_search_first(_bus, &search_state, &found);
        while (found){
            _rom_codes[_ndevices] = search_state.rom_code;
            ++_ndevices;
            owb_search_next(_bus, &search_state, &found);
        }
    }
    void init(){
        if(_ndevices > 1){
            for(int i = 0; i < _ndevices; ++i){
                DeviceT::setup(_rom_codes[i], false);
            }
        }else{
//             OneWireBus_ROMCode rom;
//             owb_status status = owb_read_rom(_bus, &rom);
            DeviceT::setup(_rom_codes[0], true);
        }
    }
    std::string signature(int index){
        OneWireBus_ROMCode code = _rom_codes[index];
        char rom_code_s[17];
        owb_string_from_rom_code(code, rom_code_s, sizeof(rom_code_s));
        return std::string(rom_code_s);
    }
    void fetch(){
        return DeviceT::read_all();
    }
    data_type at(std::size_t index) const{
        return DeviceT::at(index);
    }
    data_type at(const std::string signature) const{
        return DeviceT::at(signature);
    }
    data_type operator[](const std::string& signature) const{
        return at(signature);
    }
    data_type operator[](std::size_t index) const{
        return at(index);
    }
    template <typename StreamT>
    StreamT& write(StreamT& stream){
        DeviceT::write(stream);
        return stream;
    }
    ~onewire(){
        owb_uninitialize(_bus);
    }
};

template <typename StreamT, typename DeviceT>
StreamT& operator<<(StreamT& stream, onewire<DeviceT>& wire){
  wire.write(stream);
  return stream;
}

}

}

#endif // WATER_ONEWIRE_H
